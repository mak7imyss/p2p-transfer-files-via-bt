const config = require('@tinkoff/prettier-config');

module.exports = {
  ...config,
};
